package alaposztalyok;

import java.util.Random;

public class Palyazat {

	private static int palyazatiOsszegHatar;

	private static int egyediSorszam;

	private int sorszam;

	private Palyazo palyazo;

	public Palyazat(Palyazo palyazo) {
		this.palyazo = palyazo;
		egyediSorszam++;
		this.sorszam = egyediSorszam ;
	}

	/**
	 * @return Ha az osszeghat�r felett gener�l sz�mot akkor 0-t ad vissza.
	 */
	public int folyosithatoOsszeg() {
			return 0;
	}

	public Palyazo getPalyazo() {
		return palyazo;
	}

	public int getSorszam() {
		return sorszam;
	}

	public static int getPalyazatiOsszegHatar() {
		return palyazatiOsszegHatar;
	}

	public static void setPalyazatiOsszegHatar(int hatar) {
		palyazatiOsszegHatar = hatar;
	}

	@Override
	public String toString() {
		return (this.sorszam + ". " + this.palyazo.toString());
	}
}
