package alaposztalyok;

public class PalyazatTanulmanyi extends Palyazat {

	private int kertOsszeg;

	public PalyazatTanulmanyi(Palyazo palyazo, int kertOsszeg) {
		super(palyazo);
		this.kertOsszeg=kertOsszeg;
	}


	@Override
	public int folyosithatoOsszeg() {
		if(getPalyazo().isTanariAjanlas()){
			return kertOsszeg>getPalyazatiOsszegHatar()?getPalyazatiOsszegHatar():kertOsszeg;
		} else {
			return 0;
		}
	}
}
