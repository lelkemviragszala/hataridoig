package alaposztalyok;

public class PalyazatSport extends Palyazat{

	private int merkozesekSzama;
	private static int merkozesenkentiDij;

	public PalyazatSport(Palyazo palyazo) {
		super(palyazo);
	}

	@Override
	public int folyosithatoOsszeg() {
		if(getPalyazo().isEdzoiAjanlas()){
			int osszeg=merkozesenkentiDij*getPalyazo().getMerkozesekSzama();
			return osszeg>getPalyazatiOsszegHatar()?getPalyazatiOsszegHatar():osszeg;
		} else {
			return 0;
		}
	}

	public static int getMerkozesenkentiDij() {
		return merkozesenkentiDij;
	}

	public static void setMerkozesenkentiDij(int mERKOZESENKENTI_DIJ) {
		merkozesenkentiDij = mERKOZESENKENTI_DIJ;
	}

}
