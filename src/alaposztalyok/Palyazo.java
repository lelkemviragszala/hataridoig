package alaposztalyok;

public class Palyazo {

    private String nev;

    private String kod;

    private int okossagiSzint;

    private boolean tanariAjanlas;

    private int merkozesekSzama;

    private boolean edzoiAjanlas;

    private static int merkozesSzamHatar;
    
    private static int okossagHatar;

    public Palyazo(String nev, String kod) {
        this.nev = nev;
        this.kod = kod;
        okossagiSzint = 0;
        merkozesekSzama = 0;

    }

    public void tanul() {
        okossagiSzint++;
        if (okossagiSzint == okossagHatar) {
            tanariAjanlas = true;
        }
    }

    public void sportol() {
        merkozesekSzama++;
        if (merkozesekSzama == merkozesSzamHatar) {
            edzoiAjanlas = true;
        }
    }

    public static int getMerkozesSzamHatar() {
        return merkozesSzamHatar;
    }

    public String getNev() {
        return nev;
    }

    public int getOkossagiSzint() {
        return okossagiSzint;
    }

    public int getMerkozesekSzama() {
        return merkozesekSzama;
    }

    public String getKod() {
        return kod;
    }

    public static int getOkossagHatar() {
        return okossagHatar;
    }

    public boolean isTanariAjanlas() {
        return tanariAjanlas;
    }

    public boolean isEdzoiAjanlas() {
        return edzoiAjanlas;
    }

    public static void setMerkozesSzamHatar(int hatar) {
        merkozesSzamHatar = hatar;
    }

    public static void setOkossagHatar(int hatar) {
        okossagHatar = hatar;
    }

    @Override
    public String toString() {
        return (this.getNev() + " (" + this.getKod() + ")"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        return obj.toString().equals(this.toString());
    }

}
