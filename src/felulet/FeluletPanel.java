/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package felulet;

import alaposztalyok.Palyazat;
import alaposztalyok.PalyazatSport;
import alaposztalyok.PalyazatTanulmanyi;
import alaposztalyok.Palyazo;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.DefaultListModel;

/**
 *
 * @author Administrator
 */
public class FeluletPanel extends javax.swing.JPanel {

    static int szorgalmiIdoszakCiklusMerete = 50;
    private List<Palyazat> palyazatok = new ArrayList<>();
    private List<Palyazo> palyazottak = new ArrayList<>();
    private DefaultListModel<Palyazo> palyazoModel = new DefaultListModel<>();
    private DefaultListModel<Palyazat> palyazatokModel = new DefaultListModel<>();
    private String PALYAZOK_TXT_ELERESE = "/txt/palyazok.txt";

    /**
     * Creates new form FeluletPanel
     */
    public FeluletPanel() {
        initComponents();
        jListPalyazok.setModel(palyazoModel);
    }
    
    private void vezerles() {
        txtBeolvasasa();
        palyazas();
        Palyazo.setMerkozesSzamHatar(4);
        PalyazatSport.setMerkozesenkentiDij(1000);
        Palyazo.setOkossagHatar(5);
        Palyazat.setPalyazatiOsszegHatar(20000);
        for (int i = 0; i < szorgalmiIdoszakCiklusMerete; i++) {
            szorgalmiIdoszak();
        }

        for (Palyazat palyazat : palyazatok) {
            if (palyazat.folyosithatoOsszeg() > 0) {
                palyazatokModel.addElement(palyazat);
            }
        }
    }

    private void txtBeolvasasa() {
        try (BufferedReader bemenet = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(PALYAZOK_TXT_ELERESE)))) {
            String sor;
            while ((sor = bemenet.readLine()) != null) {
                sor = sor.trim();
                String[] palyazoAdatai = sor.split(";");
                String nev = palyazoAdatai[0];
                String kod = palyazoAdatai[1];
                Palyazo palyazo = new Palyazo(nev, kod);
                palyazoModel.addElement(palyazo);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void palyazas() {
        vezerles();
        int osszeg = 0;
        for (int i = 0; i < palyazatokModel.size(); i++) {
            osszeg += palyazatokModel.getElementAt(i).folyosithatoOsszeg();
        }
        jLabelOsszesKifiz.setText("Az összes kifizetés: " + osszeg);
        int maxFolyositas = 0;
        for (int i = 0; i < palyazatokModel.size(); i++) {
            Palyazat palyazat = palyazatokModel.get(i);
            if (palyazat.folyosithatoOsszeg() > maxFolyositas) {
                maxFolyositas = palyazat.folyosithatoOsszeg();
            }
        }
        jLabelLegnagyobbKifiz.setText("A legnagyobb kifizetés " + maxFolyositas);
    }

   
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListPalyazok = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        btnElbiralas = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListPalyazatok = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jLabelOsszesKifiz = new javax.swing.JLabel();
        jLabelLegnagyobbKifiz = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(600, 400));

        jScrollPane1.setViewportView(jListPalyazok);

        jLabel1.setText("Pályázók");

        btnElbiralas.setText("Elbírálás");
        btnElbiralas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnElbiralasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnElbiralas)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 43, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1)
                .addGap(18, 18, 18)
                .addComponent(btnElbiralas)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jListPalyazatok);

        jLabel2.setText("Pályázatok");

        jLabelOsszesKifiz.setText("Az összes kifizetés:");

        jLabelLegnagyobbKifiz.setText("A legnagyobb kifizetés:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelOsszesKifiz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelLegnagyobbKifiz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabelOsszesKifiz)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelLegnagyobbKifiz)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnElbiralasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnElbiralasActionPerformed

        jListPalyazatok.setModel(palyazatokModel);
    }//GEN-LAST:event_btnElbiralasActionPerformed


    private void szorgalmiIdoszak() {
        Random rand = new Random();
        Palyazo elsopalyazo = palyazoModel.get(rand.nextInt(palyazoModel.size()));
        Palyazo masodikPalyazo = palyazoModel.get(rand.nextInt(palyazoModel.size()));
        elsopalyazo.sportol();
        masodikPalyazo.tanul();

        if (elsopalyazo.isEdzoiAjanlas() && !palyazottak.contains(elsopalyazo)) {
            PalyazatSport sportPalyazat = new PalyazatSport(elsopalyazo);
            palyazatok.add(sportPalyazat);
            palyazottak.add(elsopalyazo);
        }
        if (masodikPalyazo.isTanariAjanlas() && !palyazottak.contains(masodikPalyazo)) {
            palyazottak.add(masodikPalyazo);
            PalyazatTanulmanyi tanulmanyiPalyazat = new PalyazatTanulmanyi(masodikPalyazo, rand.nextInt(2 * Palyazat.getPalyazatiOsszegHatar()));
            palyazatok.add(tanulmanyiPalyazat);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnElbiralas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelLegnagyobbKifiz;
    private javax.swing.JLabel jLabelOsszesKifiz;
    private javax.swing.JList<Palyazat> jListPalyazatok;
    private javax.swing.JList<Palyazo> jListPalyazok;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
